package utils;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class DataInputProvider {
public static Object[][] readExcel(String dataSheetName) throws IOException
		{
		 XSSFWorkbook wb=new XSSFWorkbook("./data/"+dataSheetName+".xlsx"); //Go to the Excel Sheet
		 System.out.println("./data/"+dataSheetName+".xlsx");
		 XSSFSheet sheet=wb.getSheetAt(0); //Access the Sheet
		 
		 int rowCount=sheet.getLastRowNum(); //Read the number of Rows
		 System.out.println("Number Of Rows"+ rowCount);
		 int cellCount=sheet.getRow(0).getLastCellNum();
		 String data[][]=new String[rowCount][cellCount];
		 System.out.println("Number Of columns"+ cellCount);
		 for( int i=1;i<=rowCount;i++)
		 { for(int j=0;j<cellCount;j++)
			 { //System.out.print(" "+sheet.getRow(i).getCell(j).getStringCellValue());
			  data[i-1][j]=sheet.getRow(i).getCell(j).getStringCellValue();
			  System.out.println(i+" "+j+" "+data[i-1][j]);
			 }
		 }
		 wb.close();
		 return data;
		
		}
	}




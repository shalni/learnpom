package TestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.LoginPage;
import wdMethods.ProjectMethods;

public class LoginLogout extends ProjectMethods{

	@BeforeTest
	public void setData()
	{String testcaseName="TC001_Login_Logout";
	 String testcaseDescription="Login into Leaftap";
	 String author="Shalni";
	 String category="AC Execution";
	 dataSheetName="TC001";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test(dataProvider="fetchData")
	public void loginLogout(String uName, String password) 
	{
	 new LoginPage()
	 .enterUserName(uName)
	 .enterPassword(password)
	 .clickLogin();
	}
	
	
}

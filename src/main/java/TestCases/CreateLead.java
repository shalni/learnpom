package TestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLead  extends ProjectMethods{
	
	
	@BeforeTest
	public void setData()
	{String testcaseName="TC002_CreateLead";
	 String testcaseDescription="Create lead";
	 String author="Shalni";
	 String category="AC Execution";
	 dataSheetName="CreateLead";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test(dataProvider="fetchData" )
	public void createLead(String cname,String fname, String lname,String email, String phone) 
	{
		new LoginPage()
		 .enterUserName("DemoSalesManager")
		 .enterPassword("crmsfa")
		 .clickLogin()
		 .clickeleCRMSFA()
		 .clickeleLeads()
		 .clickeleCreateLead()
		 .enterCompanyName(cname)
		 .enterFirstName(fname)
		 .enterLastName(lname)
		 .enterEmail(email)
		 .enterPhone(phone)
		 .clickcreateLeadButton();
	}
}

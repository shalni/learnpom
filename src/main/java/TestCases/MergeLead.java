package TestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.FindLeadPage;
import pageObjects.LoginPage;
import pageObjects.MergeLeadPage;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class MergeLead extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{String testcaseName="TC003_MergeLead";
	 String testcaseDescription="Merge lead";
	 String author="Shalni";
	 String category="AC Execution";
	 dataSheetName="MergeSheet";
	 runTestCase(testcaseName,testcaseDescription,author,category);
	}
	
	@Test(dataProvider="fetchData")
	public void mergeLead(String fname1, String fname2) throws InterruptedException
	{
		 new LoginPage()
		 .enterUserName("DemoSalesManager")
		 .enterPassword("crmsfa")
		 .clickLogin()
		 .clickeleCRMSFA()
		 .clickeleLeads()
		 .clickeleMergeLead()
		 .clickfirstLookupImage()
		 .enterFirstName(fname1)
		 .clickfindLeadButton()
		 .clicksecondLookupImage()
		 .enterFirstName(fname2)
		 .clickfindLeadButton()
		 .clickmergeButton();
		  acceptAlert();
	}
	
}



package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {

	MyLeadsPage()
	{PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//a[contains(@href,'createLeadForm')]") WebElement eleCreateLead;
	@FindBy(how=How.XPATH, using="//a[contains(@href,'findLeads')]") WebElement eleFindLead;
	@FindBy(how=How.XPATH, using="//a[contains(@href,'mergeLeads')]") WebElement eleMergeLead;
	
	public CreateLeadPage clickeleCreateLead()
	{
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	public FindLeadPage clickeleFindLead()
	{
		click(eleFindLead);
		return new FindLeadPage();
	}
	
	public MergeLeadPage clickeleMergeLead()
	{
		click(eleMergeLead);
		return new MergeLeadPage();
	}
}

package pageObjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {
	public MergeLeadPage()
	{PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how=How.XPATH, using="(//img[@alt='Lookup'])[1]")WebElement firstLookupImage;
	@FindBy(how=How.XPATH, using="(//img[@alt='Lookup'])[2]")WebElement secondLookupImage;
	@FindBy(how=How.LINK_TEXT, using="Merge")WebElement mergeButton;
	
	public FindLeadPage clickfirstLookupImage() throws InterruptedException
	{
		click(firstLookupImage);
		switchToWindow(1);
		return new FindLeadPage();
	}
	
	public FindLeadPage clicksecondLookupImage()
	{
		click(secondLookupImage);
		switchToWindow(1);
		return new FindLeadPage();
	}
	
	public ViewLeadPage clickmergeButton() throws InterruptedException
	{   
		try {
			click(mergeButton);
		} 
		catch (UnhandledAlertException f) {
		    try {
		        acceptAlert();
		    } catch (NoAlertPresentException e) {
		        e.printStackTrace();
		    }
			
	}
		System.out.println(driver.getTitle());
		return new ViewLeadPage();

	}
}

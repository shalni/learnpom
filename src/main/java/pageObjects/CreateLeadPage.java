package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	CreateLeadPage()
	{PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.ID, using="createLeadForm_primaryEmail") WebElement eleEmail;
	@FindBy(how=How.ID, using="createLeadForm_primaryPhoneNumber") WebElement elePhone;
	@FindBy(how=How.XPATH, using="createLeadForm_dataSourceId") WebElement eleSource;
	@FindBy(how=How.NAME, using="submitButton") WebElement createLeadButton;
	
	
	public CreateLeadPage enterCompanyName(String cName)
	{
		type(eleCompanyName,cName);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String fName)
	{
		type(eleFirstName,fName);
		return this;
	}
	
	public CreateLeadPage enterLastName(String lName)
	{
		type(eleLastName,lName);
		return this;
	}
	
	public CreateLeadPage enterEmail(String email)
	{
		type(eleEmail,email);
		return this;
	}
	
	public CreateLeadPage enterPhone(String phone)
	{
		type(elePhone,phone);
		return this;
	}
	
	public ViewLeadPage clickcreateLeadButton()
	{
		click(createLeadButton);
		return new ViewLeadPage();
	}
	
	
}

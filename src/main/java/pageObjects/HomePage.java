package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	HomePage()
	{PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	
	@FindBy(how=How.XPATH, using="//a[contains(@href,'crmsfa')]") WebElement eleCRMSFA;
	
	public LoginPage clickLogout()
	{
		click(eleLogout);
		//eleLogout.click();
		return new LoginPage();
	}
	
	public MyHomePage clickeleCRMSFA()
	{
		click(eleCRMSFA);
		return new MyHomePage();
	}
	




}

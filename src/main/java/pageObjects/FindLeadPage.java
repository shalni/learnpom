package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPage  extends ProjectMethods{
	
	public FindLeadPage()
	{PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.NAME, using="firstName")WebElement eleFname;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")WebElement findLeadButton;
	@FindBy(how=How.XPATH, using="(//table[@class='x-grid3-row-table'])[1]/tbody/tr")List<WebElement> allRows;
	
	
	@FindBy(how=How.XPATH, using="(//img[@alt='Lookup'])[2]")WebElement secondLookupImage;
	@FindBy(how=How.LINK_TEXT, using="Merge")WebElement mergeButton;
	
	
	
	
	public MergeLeadPage clickfindLeadButton() throws InterruptedException
	{   
		click(findLeadButton);
		Thread.sleep(2000);
		WebElement eachLeadRow=allRows.get(0);
		List<WebElement> allLeadColumns=eachLeadRow.findElements(By.tagName("a"));
		System.out.println("Column Name"+allLeadColumns.get(0).getText());
		click(allLeadColumns.get(0));
		switchToWindow(0);
		System.out.println(driver.getTitle());
		return new MergeLeadPage();
	
	}
	
	public FindLeadPage enterFirstName(String fName) throws InterruptedException
	{     Thread.sleep(2000);
		System.out.println("fanem"+fName);
		eleFname.sendKeys(fName);
		//type(fname,fName);
		return this;
	}


	
}

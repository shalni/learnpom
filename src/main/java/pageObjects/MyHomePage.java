package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods {
	MyHomePage()
	{PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//a[contains(@href,'leadsMain')]") WebElement eleLeads;
	
	public MyLeadsPage clickeleLeads()
	{
		click(eleLeads);
		return new MyLeadsPage();
	}
	
}

package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {
	public LoginPage()
	{PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="username") WebElement eleUsername;
	@FindBy(how=How.ID,using="password") WebElement elepassword;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;
	
	public LoginPage enterUserName(String uName)
	{
		
		type(eleUsername,uName);
		return this;
	}
	
	public LoginPage enterPassword(String password)
	{ 
		type(elepassword,password);
		return this;
	}
	
	public HomePage clickLogin()
	{
		click(eleLogin);
		return new HomePage();
	}


}

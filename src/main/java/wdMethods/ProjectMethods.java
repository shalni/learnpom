package wdMethods;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.DataInputProvider;

import org.testng.annotations.AfterMethod;

import wdMethods.SeMethods;

public class ProjectMethods extends SeMethods{
	
	public String dataSheetName;
	
	//@Parameters({"url","username","password"})
	@BeforeMethod
	public void login() {
		startApp("chrome","http://leaftaps.com/opentaps");
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword,password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crmsfaClick = locateElement("xpath","//a[contains(@href,'crmsfa')]");
		click(crmsfaClick);
		WebElement leadPage = locateElement("xpath","//a[contains(@href,'leadsMain')]");
		leadPage.click();*/
	}
	
	@AfterMethod
	public void closeApp()
	{closeBrowser();	
	}
	
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException 
	{return(DataInputProvider.readExcel(dataSheetName));
	 //Object data[][]
	}
}












